import { Component, OnInit } from '@angular/core';
import { Repo } from 'src/app/models/repo';
import { GithubServices } from 'src/app/services/github.services';
import { UpLoadService } from '../../services/upload.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
  providers: [UpLoadService, GithubServices]
})
export class ProjectsComponent implements OnInit {

  title = 'cloudinary';
  image: File = null;
  githubData: [] = null;
  githubRepos: Repo[] = [];

  constructor(private _uploadService: UpLoadService, private _githubService: GithubServices) {
    this.githubData = JSON.parse(localStorage.getItem('GithubCredentials'));
  }

  //upload to cloudinary
  onUpload(event) {
    const USER: string = "Test";

    if (!event.files[0]) {
      console.log("Subir una imagen png o jpeg");
      alert("Subir una imagen png o jpeg");
    } else {
      const file_data = event.files[0];
      const data = new FormData(); //crea un objeto de tuipo data para manejarlo por peticiones REST
      data.append('file', file_data);  //seteamos a nustro objeto nuestro arreglo de imagenes
      data.append('upload_preset', 'angular_cloudinary'); //ademas indicamos nuestra ubicacion (preset cloudinary)
      data.append('cloud_name', 'dkkhfbnok');
      data.append("public_id", `${USER}/test`);

      this._uploadService.UploadImage(data).subscribe((response) => {
        if (response) {
          console.log(response);
          console.log(response.secure_url);
        }
      })
    }
  }

  ngOnInit(): void {
    if (this.githubData != null) {
      this._githubService.GetRepositories().subscribe((response) => {
        if (response != null) {
          for (const i in response) {
            this.githubRepos.push({
              id: response[i].id,
              name: response[i].name,
              full_name: response[i].full_name,
              description: response[i].description,
              owner: response[i].owner.login,
              html_url: response[i].html_url,
              created_at: response[i].created_at,
              updated_at: response[i].updated_at,
              language: response[i].language,
              avatar: response[i].owner.avatar_url,
            })
          }
        }
      });
    }
  }

}
