import { Component } from '@angular/core';
import { Login } from 'src/app/models/login';
import { ApiServices } from 'src/app/services/api.services';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MessageService],
})
export class LoginComponent {
  public username: string;
  public email: string;
  public password: string;
  public passwordConfirm: string;
  public buscar: string;

  constructor(
    public apiService: ApiServices,
    private messageService: MessageService,
    private router: Router
  ) {
    if (this.apiService.usuarioData) this.router.navigate(['/profile']);
  }

  registerUser() {
    if (this.password === this.passwordConfirm) {
      const login: Login = {
        username: this.username,
        email: this.email,
        password: this.password,
      };
      this.apiService.RegisterCliente(login).subscribe((response) => {
        if (response.success) {
          this.messageService.add({
            severity: 'success',
            summary: 'Gracias por registrarte',
            detail: response.message,
          });
        } else {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: response.message,
          });
        }
      });
    } else {
      this.messageService.add({
        severity: 'error',
        summary: 'Contraseñas diferentes',
        detail: 'Las contraseñas colocadas no coinciden intentelo de nuevo',
      });
    }
  }

  loginUser() {
    const login: Login = {
      username: '',
      email: this.email,
      password: this.password,
    };
    this.apiService.LoginCliente(login).subscribe((response) => {
      if (response.success) {
        this.messageService.add({
          severity: 'success',
          summary: 'Bienvenido',
          detail: response.message,
        });
        this.router.navigate(['/profile']);
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Error',
          detail: response.message,
        });
      }
    });
  }

  searchUser() {
    console.log(this.buscar);
  }
}
