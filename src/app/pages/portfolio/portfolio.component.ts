import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  public projects:Project[] = [];

  constructor() { }

  ngOnInit(): void {
    this.projects = [
      {
        id: 1,
        title: "Angular Test",
        gitLink: "https://gitlab.com/NOTCompile/angulartest",
        description: "Poryecto para Mayta, mucho caso y destruccion",
        image: "https://miro.medium.com/max/3000/1*-f6StBRZQiuOAz7fdqWFKw.png",
        fecha: new Date(),
        mainTag: "Angular",
        stars: 5
      },
      {
        id: 2,
        title: "Proyecto UFolio 2",
        gitLink: "https://victorroblesweb.es/wp-content/uploads/2018/11/angular-bg.png",
        description: "Poryecto para Mayta, mucho caso y destruccion",
        image: "https://victorroblesweb.es/wp-content/uploads/2018/11/angular-bg.png",
        fecha: new Date(),
        mainTag: "Angular",
        stars: 4
      },
      {
        id: 3,
        title: "Proyecto sirvepe",
        gitLink: "https://i.morioh.com/51d7987e51.png",
        description: "Poryecto para Mayta, mucho caso y destruccion",
        image: "https://res.cloudinary.com/dkkhfbnok/image/upload/v1610839434/testing_angular_cloudinary/test.png",
        fecha: new Date(),
        mainTag: "Angular",
        stars: 3
      }
    ]
  }

}
