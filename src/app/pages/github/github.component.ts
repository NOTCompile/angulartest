import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UpLoadService } from 'src/app/services/upload.service';

@Component({
  selector: 'app-github',
  templateUrl: './github.component.html',
  providers: [UpLoadService]
})
export class GithubComponent implements OnInit {
  code: string;
  constructor(private route: ActivatedRoute, private _uploadService: UpLoadService, private router: Router) { }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.code = params.code;
      }
      );
    this._uploadService.GithubHanlder(this.code).subscribe((response) => {
      if (response.success) {
        localStorage.setItem('GithubCredentials', JSON.stringify(response.data));
      }
      this.router.navigate(['/projects']);
    })
  }
}