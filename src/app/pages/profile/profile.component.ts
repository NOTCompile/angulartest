import { Component, OnInit } from '@angular/core';
import { ApiServices } from 'src/app/services/api.services';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(
    public apiService: ApiServices
  ) { }

  geUsers() {
    this.apiService.GetUsers().subscribe((response) => {
      console.log(response)
    });
  }

  ngOnInit(): void {
  }
}