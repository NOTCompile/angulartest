import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Login } from '../models/login';
import { Resp } from '../models/resp';
import { User } from '../models/user';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({ providedIn: 'root' })
export class ApiServices {
  url: string = 'https://localhost:44347/api';

  private userSubject: BehaviorSubject<User>;
  public userObs: Observable<User>;

  constructor(private http: HttpClient) {
    this.userSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem('User'))
    );
    this.userObs = this.userSubject.asObservable();
  }

  public get usuarioData(): User {
    return this.userSubject.value;
  }

  RegisterCliente(user: Login): Observable<Resp> {
    return this.http.post<Resp>(`${this.url}/Auth/Register`, user, httpOptions);
  }

  LoginCliente(user: Login): Observable<Resp> {
    return this.http
      .post<Resp>(`${this.url}/Auth/Login`, user, httpOptions)
      .pipe(
        map((res) => {
          if (res.success) {
            const user: User = res.data;
            localStorage.setItem('User', JSON.stringify(user));
            this.userSubject.next(user);
          }
          return res;
        })
      );
  }

  Logout() {
    localStorage.removeItem('User');
    this.userSubject.next(null);
  }

  GetUsers() {
    return this.http.get(`${this.url}/User`, httpOptions);
  }
}
