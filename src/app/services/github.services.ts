import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class GithubServices {
    url: string = 'https://api.github.com';
    httpOptions: any = null;

    constructor(private _http: HttpClient) {
        const TOKEN = JSON.parse(localStorage.getItem('GithubCredentials'));
        if (TOKEN != null) {
            this.httpOptions = {
                headers: new HttpHeaders({
                    'Authorization': 'token ' + TOKEN.access_token
                }),
            };
        }
    }

    GetRepositories(): Observable<any> {
        return this._http.get(`${this.url}/user/repos`, this.httpOptions);
    }
}
