import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class UpLoadService {
  constructor(private _http: HttpClient) { }

  //tratar de utilizar Response y no ambos, quizas nos saca el error por eso
  UploadImage(vals): Observable<any> {
    let data = vals;
    //for cloudinary
    return this._http.post(
      'https://api.cloudinary.com/v1_1/dkkhfbnok/image/upload', data);
  }

  GithubHanlder(code:string):Observable<any> {
    return this._http.post(
      `http://localhost:3000/github/${code}`, null);
  }
}
