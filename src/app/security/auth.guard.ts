import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { ApiServices } from '../services/api.services';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private apiServices: ApiServices) {}

  canActivate(route: ActivatedRouteSnapshot) {
    const user = this.apiServices.usuarioData;
    if (user != null) {
        return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}
