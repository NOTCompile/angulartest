import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ApiServices } from '../services/api.services';
import { catchError } from 'rxjs/operators';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private apiServices: ApiServices) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const user = this.apiServices.usuarioData;
    if (user != null) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${user.token}`
        }
      });
    }
    //return next.handle(request);
    return next.handle(request).pipe(
      catchError((response: HttpErrorResponse) => {
        /* quitarlo cuando termine de implementarse github
        if (response.status === 401 || response.status === 403) {
          localStorage.removeItem('User');
          localStorage.removeItem('GithubCredentials');
          window.location.href = '/login';
          alert("Tu sesión ha expirado");
        }*/
        return throwError(response);
      })
    );
  }
}