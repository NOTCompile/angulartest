import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { from } from 'rxjs';
//import primeng components
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { FieldsetModule } from 'primeng/fieldset';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ToastModule } from 'primeng/toast';
import { PasswordModule } from 'primeng/password';
import { TabViewModule } from 'primeng/tabview';
import { ChipModule } from 'primeng/chip';
import {FileUploadModule} from 'primeng/fileupload';
//imports components
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CardComponent } from './components/card/card.component';
//import pages
import { IndexComponent } from './pages/index/index.component';
import { ErrorComponent } from './pages/error/error.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
//layouts
import { MainLayoutComponent } from './_layout/main-layout/main-layout.component';
//services and others
import { JwtInterceptor } from './security/jwt.interceptor';
import { RepoViewComponent } from './components/repo-view/repo-view.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    ErrorComponent,
    LoginComponent,
    ProfileComponent,
    SidebarComponent,
    CardComponent,
    ProjectsComponent,
    PortfolioComponent,
    MainLayoutComponent,
    RepoViewComponent,
    RegisterFormComponent,
  ],
  imports: [
    BrowserModule,
    // import HttpClientModule after BrowserModule.
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CardModule,
    FieldsetModule,
    InputTextModule,
    ButtonModule,
    InputTextareaModule,
    ToastModule,
    FormsModule,
    PasswordModule,
    TabViewModule,
    ChipModule,
    FileUploadModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
