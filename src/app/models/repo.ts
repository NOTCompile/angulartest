export interface Repo {
    id:number;
    name:string;
    full_name:string;
    description?:string;
    owner:string;
    html_url:string;
    created_at:string;
    updated_at:string;
    language:string;
    avatar:string;
}