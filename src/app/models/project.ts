export interface Project {
    id:number;
    title:string;
    description:string;
    fecha:Date;
    mainTag:string;
    stars:number;
    gitLink?:string;
    image?:string;
}