import { Component, Input, OnInit } from '@angular/core';
import { Repo } from 'src/app/models/repo';

@Component({
  selector: 'app-repo-view',
  templateUrl: './repo-view.component.html',
  styleUrls: ['./repo-view.component.css']
})
export class RepoViewComponent implements OnInit {
  @Input() repo:Repo; //get repo data

  constructor() { }

  ngOnInit(): void {
  }

}