import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './models/user';
import { ApiServices } from './services/api.services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  usuario: User;
  title: string;
  
  constructor(
    public apiServices: ApiServices,
    public router: Router
  ) {
    this.apiServices.userObs.subscribe(res => {
      this.usuario = res;
      console.log("Objeto User cambiado: " + res);
    })
  }

  logout() {
    this.apiServices.Logout();
    this.router.navigate(['/login']);
  }

  githubUrl: string = 'https://github.com/login/oauth/authorize?client_id=' + 'b2a6f5eb261082f5391a';
}
